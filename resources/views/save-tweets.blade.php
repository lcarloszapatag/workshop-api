<!DOCTYPE html>
<html>
<head>
    <title>Save Tweets</title>
</head>
<body>
        <h3>Save Tweets</h3>
        <form method="post" action="{{route('save.tweet')}}">
            {{ csrf_field() }}
            <label>Account:</label>
            <input type="input" name="account" id="account">
            <label>Tweet:</label>
            <textarea id="tweet" name="tweet" cols="30" rows="5"></textarea>
            <label>Date:</label>
            <!--<input type="datetime-local" name="tweeted_at" id="tweeted_at">-->
            <input type="date" name="tweeted_at" id="tweeted_at">
            <button type="submit">Guardar</button>
        </form>
        <br />
</body>
</html>
